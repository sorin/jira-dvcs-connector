Overview
========

With the Bitbucket and JIRA plugin you can create unlimited private repositories with full-featured issue and project management support. And it takes less than 30 seconds to configure. Bring your code to the next level with Bitbucket and JIRA.

* `30 second overview video`_
* `Install the JIRA DVCS Connector`_

.. image:: http://blog.bitbucket.org/wp-content/uploads/2011/06/Bitbucket-JIRA-tab1.png
    :align: center

Features
========

* Track changesets, monitor source code edits, and link back to Bitbucket.
* Push your changesets to JIRA by referencing issue keys in your commit messages.
* Map unlimited public and private Bitbucket repositories to your JIRA projects. 
* Full setup and configuration in under 1 minute.

.. _`Install the JIRA DVCS Connector`: https://plugins.atlassian.com/plugin/details/311676
.. _`30 second overview video`: http://www.youtube.com/watch?v=7Eeq_87y3NM

Development Guide
=================

Issue Tracking
--------------

+-----------------------+----------------------------------------+
| Public facing:        | https://jira.atlassian.com/browse/DCON |
+-----------------------+----------------------------------------+
| Internal development: | https://jdog.jira-dev.com/browse/BBC   |
+-----------------------+----------------------------------------+



Development Branches
--------------------
Basically new work should go on master.

.. |master.png| image:: https://drone.io/bitbucket.org/sorin/jira-dvcs-connector/status.png
 :target: https://drone.io/bitbucket.org/sorin/jira-dvcs-connector/latest?branch=master

.. |jira6.3.x.png| image:: https://drone.io/bitbucket.org/sorin/jira-dvcs-connector/status.png
 :target: https://drone.io/bitbucket.org/sorin/jira-dvcs-connector/latest?branch=jira6.3.x
    
+-----------------+-------------+-------------------------+----------------+
|Branch           | Versions    | Supported JIRA Versions | Status         |
+=================+=============+=========================+================+
| master          | 3.3.x       | 6.4.x                   ||master.png|    | 
+-----------------+-------------+-------------------------+----------------+
| jira6.3.x       | 2.1.x       | 6.3.x                   ||jira6.3.x.png| |
+-----------------+-------------+-------------------------+----------------+

Building the code
========
The integration tests use a specific user account in Bitbucket whose credentials are not available, also they take around an hour to run. If you want to compile the code you can use ```atlas-mvn clean install -DskipITs=true``` which will run the unit test suite.

Releasing
=======
Skip the tests as they take too long - ```atlas-mvn release:clean release:prepare release:perform -DskipTests -Darguments="-DskipTests" -DautoVersionSubmodules=true```